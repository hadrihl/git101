Git101
======
A dummy project for practicing Git.

How to compile
--------------
$ gcc hello.c -o a.out

How to run
----------
$ ./a.out

Author
------
hadrihl // hadrihilmi@gmail.com
